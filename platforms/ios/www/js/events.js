/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        getDataList();
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        // var listeningElement = parentElement.querySelector('.listening');
        // var receivedElement = parentElement.querySelector('.received');

        // listeningElement.setAttribute('style', 'display:none;');
        // receivedElement.setAttribute('style', 'display:block;');

        // console.log('Received Event: ' + id);
    }
};

app.initialize();

function getDataList(){

    $('span.loading').show();
    var storage = window.localStorage;
    var app_id = storage.getItem("app_id");

    $.get("https://ogaticketr.com/api/mobile/check_in/"+ app_id +"/events", function (response) {
       
            var events = response.events;
            var dataBody = '';
    
            if(typeof events != 'undefined' && events != null && events.length != null && events.length > 0) {
                for (let i = 0; i < events.length; i++){
                    dataBody += '<tr>';
                    dataBody += '<td colspan="4">' + events[i]['title'] + '</td></tr>';
                    dataBody += '<tr><td colspan="4">' + events[i]['venue_name'] + '</td></tr>';
                    dataBody += '<td>' + events[i]['start_date'] + '</td>';
                    dataBody += '<td>' + events[i]['end_date'] + '</td>';
                    dataBody += '<td colspan="4"><button type="button" class="btn btn-primary btn-md" onclick="checkInEvent(' + events[i]['id'] + ', \'' + events[i]['title'] + '\')">Check In</button></td>';
                    dataBody += '</tr>';
                }
            } else {
                dataBody = '<tr><td colspan="4">No events found</td></tr>';
            }
    
            $('tbody#events').html(dataBody);
            $('span.loading').hide();

    }).fail(function(error) {
		$('tbody#events').html('<tr><td colspan="4">Error! refresh</td></tr>');
	});      
}

function checkInEvent(eventId, eventName){
    var storage = window.localStorage;
    storage.setItem("event_id", eventId);
    storage.setItem("event_name", eventName);
    location.assign("eventdetail.html");
}