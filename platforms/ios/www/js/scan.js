/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        scanTickets();
    }
};

app.initialize();

function scanTickets(){

    QRScanner.scan(displayContents);  
    
    var storage = window.localStorage;

    if (storage.getItem("frontCameraEnabled") === null) {
        storage.setItem("frontCameraEnabled", 0);
    }

    var fc = storage.getItem("frontCameraEnabled");

    if(fc == 1){
        QRScanner.useFrontCamera();
    } else {
        QRScanner.useBackCamera();
    }

    if (storage.getItem("flashEnabled") === null) {
        storage.setItem("flashEnabled", 0);
    }

    var fe = storage.getItem("flashEnabled");

    if(fe == 1){
        QRScanner.enableLight();
    } else {
        QRScanner.disableLight();
    }
    
    QRScanner.show();
    $('#scanresponse').html('<h1 style="margin-top:5%;color: rgb(27, 211, 3) !important;"><i class="fa fa-spinner fa-spin"></i> scanning ...</h1>');
    
}

function cancelScan()
{
    QRScanner.cancelScan();
    location.assign("eventdetail.html");

}

function switchCamera()
{
    var storage = window.localStorage;
    if (storage.getItem("frontCameraEnabled") === null) {
        storage.setItem("frontCameraEnabled", 0);
    }

    var fc = storage.getItem("frontCameraEnabled");

    if(fc == 0){
        QRScanner.useFrontCamera();
        storage.setItem("frontCameraEnabled", 1);
    } else {
        QRScanner.useBackCamera();
        storage.setItem("frontCameraEnabled", 0);
    }

}

function switchLight()
{
    var storage = window.localStorage;
    if (storage.getItem("flashEnabled") === null) {
        storage.setItem("flashEnabled", 0);
    }

    var fe = storage.getItem("flashEnabled");

    if(fe == 0){
        QRScanner.enableLight();
        storage.setItem("flashEnabled", 1);
    } else {
        QRScanner.disableLight();
        storage.setItem("flashEnabled", 0);
    }

}

function displayContents(err, text){
    if(err){
      // an error occurred, or the scan was canceled (error code `6`)
    } else {
      // The scan completed, display the contents of the QR code:

      var event_id = storage.getItem("event_id");

        $.ajax({
            type: "POST",
            url: "https://ogaticketr.com/api/mobile/"+ event_id +"/qrcode_check_in",
            data: "attendee_reference="+ text.result,
            cache: false,
            success: function(response) {
                
                if(response.status == "error"){
                    $('#scanresponse').html('<h1 style="margin-top:5%;color: red !important;">'+ response.message +'</h1>');                    
                } else {
                    $('#scanresponse').html('<h1 style="margin-top:5%;color: rgb(27, 211, 3) !important;">'+ response.message +'</h1>');
                }
                                
            },
            error: function(error) {
                 $('#scanresponse').html('<h1 style="margin-top:5%;color: red !important;">An error occured. Try again</h1>');
            }
        });

      setTimeout(function(){
        QRScanner.scan(displayContents);
        $('#scanresponse').html('<h1 style="margin-top:5%;color: rgb(27, 211, 3) !important;"><i class="fa fa-spinner fa-spin"></i> scanning ...</h1>');
      }, 5000);
    }
  }